<?php


use App\Controllers\_Controller;


$app->group('/registrazione', function () {
    //invia richiesta con i dati registrazione
    $this->post('', _Controller::class . ':registrazione');
    //conferma la registrazione tramite link
    $this->get('/conferma[/{params:.*}]', _Controller::class . ':conferma_registrazione');

});

$app->group('/dietologo', function () {
    //azioni dietologo
    $this->post('/login', _Controller::class . ':login');
    $this->delete('/cancella',_Controller::class . ':cancella_dietologo');
    $this->put('/modificapass',_Controller::class. ':modifica_pass');
    $this->post('/caricamento',_Controller::class. ':caricamento');
    $this->delete('/eliminapaziente',_Controller::class. ':elimina_paziente');
});

$app->post('/paziente', _Controller::class. ':login_paziente')->setName('login_paziente');

?>
