<?php

namespace App\Controllers;


use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Monolog\Handler\BrowserConsoleHandler;

/**
 * Class _Controller.
 */
class _Controller
{

    public function __construct()
    { }

    public function invia_registrazione($email, $password)
    {
        // subject
        $subject = 'registrazione';

        // message
        $message = '
       <a href="http://localhost/api/public/registrazione/conferma/' . $email . '/' . $password . '">CONFERMA REGISTRAZIONE</a>
     ';

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        // Additional headers
        $headers .= 'To:' . $email . "\r\n";
        $headers .= 'From: iCare SERVICE <testphpmailapache@gmail.com>' . "\r\n";
        if (mail($email, $subject, $message, $headers)) {
            return true;
        } else {
            return false;
        }
    }

    //estrae il nome della cartella partirtendo dall'userID
    public function extract_dir($email)
    {
        $cartella = explode('@', $email);
        $cartella = $cartella[0] . $cartella[1];
        return "../app/" . $cartella;
    }

    //creazione account
    public function crea_account($email, $password)
    { //nome cartella test@email.it ->testemail
        $cartella = $this->extract_dir($email);
        if (!is_dir($cartella) && mkdir($cartella)) {
            //crea file json e lo salva nella cartella creata
            $password = hash('sha256', $password);
            $config = array('loginUsername' => $email, 'loginPassword' => $password);
            $config = json_encode($config);
            if (file_put_contents($cartella . '/config.json', $config)) {
                return 1;
            } else {
                return -2;
            }
        } else {
            return -1;
        }
    }

    //controllo per il login
    //controllo con password già codificate
    public function check_account($email, $password, $cartella)
    {
        $file = file_get_contents($cartella . "/config.json");
        $file = json_decode($file, true);
        if ($file) {
            if ($file['loginUsername'] == $email && $file['loginPassword'] == $password) {
                return 1;
            } else {
                return -6;
            }
        } else return -5;
    }

    //verifico l'esistenza della cartella
    //cancella i dati all'interno della cartella
    //cancella la cartella
    public function delete_dir($src)
    {
        switch ($this->check_account($email, $password, $src)) {
            case 1:
                if (is_dir($src)) {
                    $dir = opendir($src);
                    while (false !== ($file = readdir($dir))) {
                        if (($file != '.') && ($file != '..')) {
                            $full = $src . '/' . $file;
                            unlink($full);
                        }
                    }
                    closedir($dir);
                    rmdir($src);
                    return true;
                } else {
                    return false;
                }
                break;
            case -5:
                return -5;
            case -6:
                return -6;
            default:
                return 0;
        }
    }

    //modifica password con password già codifcate
    public function change_password($email, $password, $newpassword)
    {
        $cartella = $this->extract_dir($email);
        switch ($this->check_account($email, $password, $cartella)) {
            case -6:
                return -6;
            case -5:
                return -5;
            case 1:
                $config = array('loginUsername' => $email, 'loginPassword' => $newpassword);
                if (file_put_contents($cartella . '/config.json', json_encode($config))) {
                    return 1;
                } else {
                    return -2;
                }
            default:
                return 0;
        }
    }
    //caricamento file json 
    //da includere controllo 
    public function upload_json_file($cartella, $uploadedFiles, $email, $password)
    {
        //caricamento json tramite file
        //nome utente in base al nome del file json
        switch ($this->check_account($email, $password, $cartella)) {
            case 1:
                if ($uploadedFiles != null) {
                    foreach ($uploadedFiles as $file) {
                        $jfile = $file;
                    }
                    $filename = $jfile->getClientFilename();
                    //controllo sicurezza file
                    if (pathinfo($filename, PATHINFO_EXTENSION) == 'json') {
                        $jfile->moveTo($cartella . '/' . $filename);
                        return 1;
                    } else return -2;
                }
            case -6:
                return -6;
            case -5:
                return -5;
            default:
                return 0;
        }
    }
    //caricamento json trimite stringa
    public function upload_json_string($cartella, $par, $email, $password)
    {
        //identifico il json in base al contenuto 
        switch ($this->check_account($email, $password, $cartella)) {
            case 1:
                foreach ($par as $key => $value) {
                    if (strpos($value, 'password')) {
                        $file = $value;
                        $name = $key;
                    }
                }
                if ($file != null && $name != null) {
                    $decoded = (array)json_decode($file);
                    if (file_put_contents($cartella . '/' . $name . '.json', json_encode($decoded))) {
                        return 1;
                    } else {
                        return -2;
                    }
                } else return -10;
            case -5:
                return -5;
            case -6:
                return -6;
            default:
                return 0;
        }
    }


    //controllo per il login
    //controllo con password già codificate
    public function check_account_paziente($password, $cartella, $id_paziente)
    {
        $file = file_get_contents($cartella . "/" . $id_paziente . ".json");
        if ($file == null)
        return -11;
        else {
            $file = json_decode($file, true);
            if ($file['password'] == $password) {
                return 1;
            } else {
                return -6;
            }
        }
    }
    //cacella paziente del dietologo
    public function delete_account_paziente($password, $email, $cartella, $paziente)
    {
        switch ($this->check_account($email, $password, $cartella)) {
            case 1:
                if (file_exists($cartella . '/' . $paziente . ".json")) {
                    if (unlink($cartella . '/' . $paziente . ".json")) {
                        return 1;
                    } else -12;
                } else return -13;
            case -5:
                return -5;
            case -6:
                return -6;
        }
    }

    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //RISPOSTE
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    public function conferma_registrazione(Request $request, Response $response, $args)
    {
        $params = explode('/', $args['params']);
        $email = $params[0];
        $password = $params[1];
        /*if (isset($email, $password) && $this->crea_account($email, $password)) {
            $data = array('error' => false);
        } else {
            $data = array('error' => true);
        }*/
        if ($email && $password) {
            switch ($this->crea_account($email, $password)) {
                case -1:
                    $data = array('error' => true);
                    $data['code'] = -1;
                    break;

                case -2:
                    $data = array('error' => true);
                    $data['code'] = -2;
                    break;

                case 1:
                    $data = array('error' => false);
                    break;
                default:
                    $data = array('error' => true);
                    $data['code'] = 0;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }

    public function registrazione(Request $request, Response $response, $args)
    {
        // ricavo email-password- nome cartella per verifica
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $cartella = $this->extract_dir($email);
        /*if (isset($email, $password, $cartella) && !is_dir($cartella) && $this->invia_registrazione($email, $password)) {
            $data = array('error' => false);
        } else {
            $data = array('error' => true);
        }*/
        if ($email && $password && $cartella) {
            if (is_dir($cartella)) {
                $data = array('error' => true);
                $data['code'] = -1;
            } elseif (!$this->invia_registrazione($email, $password)) {
                $data = array('error' => true);
                $data['code'] = -4;
            } else $data = array('error' => false);
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }

    public function login(Request $request, Response $response, $args)
    {
        // ricavo email-password per la verifica
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $cartella = $this->extract_dir($email);
        if ($email && $password) {
            if (!is_dir($cartella)) {
                $data = array('error' => true);
                $data['code'] = -7;
            }
            //lancio la funzione di verifica passando email password e path 
            //della cartella del dietologolo
            else {
                switch ($this->check_account($email, $password, $cartella)) {
                    case 1:
                        $data = array('error' => false);
                        break;
                    case -5:
                        $data = array('error' => true);
                        $data['code'] = -5;
                        break;
                    case -6:
                        $data = array('error' => true);
                        $data['code'] = -6;
                        break;
                    default:
                        $data = array('error' => true);
                        $data['code'] = 0;
                }
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }

        return $response->write(json_encode($data));
    }

    public function cancella_dietologo(Request $request, Response $response, $args)
    {
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        if ($email && $password) {
            
            $src = $this->extract_dir($email);die($email.$password.$src);
            if ($this->delete_dir($email,$password,$src)) {
                $data = array('error' => false);
            } else {
                $data = array('error' => true);
                $data['code'] = -8;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }

    //modifica password dietologo
    public function modifica_pass(Request $request, Response $response, $args)
    {
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $newpassword = $request->getParsedBody()['newpassword'];
        if ($email && $password && $newpassword) {
            switch ($this->change_password($email, $password, $newpassword)) {
                case 1:
                    $data = array('error' => false);
                    break;
                case 0:
                    $data = array('error' => true);
                    $data['code'] = 0;
                    break;
                case -2:
                    $data = array('error' => true);
                    $data['code'] = -2;
                    break;
                case -5:
                    $data = array('error' => true);
                    $data['code'] = -5;
                    break;
                case -6:
                    $data = array('error' => true);
                    $data['code'] = -6;
                    break;
                default:
                    $data = array('error' => true);
                    $data['code'] = 0;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }
    public function caricamento(Request $request, Response $response, $args)
    {
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $uploadedFiles = $request->getUploadedFiles();
        $par = $request->getParsedBody();
        $cartella = $this->extract_dir($email);
        if ($email && $password && $par && $cartella) {

            if (($uploadedFiles != null)) {
                switch ($this->upload_json_file($cartella, $uploadedFiles, $email, $password)) {
                    case 1:
                        $data = array('error' => false);
                        break;
                    case -2:
                        $data = array('error' => true);
                        $data['code'] = -2;
                        break;
                    case -5:
                        $data = array('error' => true);
                        $data['code'] = -5;
                        break;
                    case -6:
                        $data = array('error' => true);
                        $data['code'] = -6;
                        break;

                    default:
                        $data = array('error' => true);
                        $data['code'] = 0;
                        break;
                }
            } elseif (($uploadedFiles == null)) {
                switch ($this->upload_json_string($cartella, $par, $email, $password)) {
                    case 1:
                        $data = array('error' => false);
                        break;
                    case -2:
                        $data = array('error' => true);
                        $data['code'] = -2;
                        break;
                    case -5:
                        $data = array('error' => true);
                        $data['code'] = -5;
                        break;
                    case -6:
                        $data = array('error' => true);
                        $data['code'] = -6;
                        break;

                    case -10:
                        $data = array('error' => true);
                        $data['code'] = -10;
                        break;
                    default:
                        $data = array('error' => true);
                        $data['code'] = 0;
                        break;
                }
            } else {
                $data = array('error' => true);
                $data['code'] = -3;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }
    public function login_paziente(Request $request, Response $response, $args)
    {
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $id_paziente = $request->getParsedBody()['id_paziente'];
        $cartella = $this->extract_dir($email);
        if ($email && $password && $id_paziente && $cartella) {
            switch ($this->check_account_paziente($password, $cartella, $id_paziente)) {
                case 1:
                    $data = array('error' => false);
                    break;
                case -6:
                    $data = array('error' => true);
                    $data['code'] = -6;
                    break;
                case -11:
                    $data = array('error' => true);
                    $data['code'] = -11;
                    break;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }
    public function elimina_paziente(Request $request, Response $response, $args)
    {
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];
        $paziente = $request->getParsedBody()['id_paziente'];
        $cartella = $this->extract_dir($email);
        if ($email && $password && $paziente && $cartella) {
            switch ($this->delete_account_paziente($password, $email, $cartella, $paziente)) {
                case 1:
                    $data = array('error' => false);
                    break;
                case -1:
                    $data = array('error' => true);
                    $data['code'] = 0;
                    break;
                case -5:
                    $data = array('error' => true);
                    $data['code'] = -5;
                    break;
                case -6:
                    $data = array('error' => true);
                    $data['code'] = -6;
                    break;
                case -12:
                    $data = array('error' => true);
                    $data['code'] = -12;
                    break;
                case -13:
                    $data = array('error' => true);
                    $data['code'] = -13;
                    break;
                default:
                    $data = array('error' => true);
                    $data['code'] = 0;
            }
        } else {
            $data = array('error' => true);
            $data['code'] = -3;
        }
        return $response->write(json_encode($data));
    }
}

 