<?php

namespace App\DataAccess;

use Psr\Log\LoggerInterface;
use PDO;

 
/**
 * Class _DataAccess.
 */
class _DataAccess
    {
     /**
      * @var \Psr\Log\LoggerInterface
      */
     private $logger;

     /**
      * @var \PDO
      */
     private $pdo;
    
     /**
      * @var \App\DataAccess
      */
     private $maintable;

    
     /**
      * @param \Psr\Log\LoggerInterface $logger
      * @param \PDO                     $pdo
      */
     public function __construct(LoggerInterface $logger, PDO $pdo, $table)
        {
         $this->logger    = $logger;
         $this->pdo       = $pdo;
         $this->maintable = $table;
        }

   
     /**
      * @param array $request_data
      *
      * @return bool
      */
     public function update($path, $args, $request_data)
        {
         $this->logger->info(substr(strrchr(rtrim(__CLASS__, '\\'), '\\'), 1).': '.__FUNCTION__);

         $table = $this->maintable != '' ? $this->maintable : $path;
       	 
         // if no data to update or not key set = return false
         if($request_data == null || !isset($args[implode(',', array_flip($args))]))
            {
             return false;
            }

         $sets = 'SET ';

         foreach($request_data as $key => $value)
            {
             $sets = $sets . $key . ' = :' . $key . ', ';
            }

         $sets = rtrim($sets, ", ");

         $id = implode(',',array_keys($args));
        
         //$args[$id] = base64_decode($args[$id]);

         $sql = "UPDATE ". $table . ' ' . $sets . ' WHERE ' . implode(',', array_flip($args)) . ' = :' . implode(',', array_flip($args));
        
         $stmt = $this->pdo->prepare($sql);

         foreach($request_data as $key => $value)
            {
            if ($request_data[$key]=='reactivation')
            {
            		$stmt->bindValue(':' . $key,NULL);
            }
            else
            { 	
             $stmt->bindValue(':' . $key,$request_data[$key]);
            }
            }
  
         // bind the key
         $stmt->bindValue(':' . implode(',', array_flip($args)), implode(',', $args));

         $stmt->execute();

      	 return ($stmt->rowCount() == 1) ? true : false;
        }
	 }

?>