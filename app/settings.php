<?php

return
    [
     'settings' =>
        [
         // Slim Settings
         'determineRouteBeforeAppMiddleware' => false,
         'displayErrorDetails' => true,
         'addContentLengthHeader' => false,
         'PoweredBy' => '',

         // database settings
         'pdo' =>
            [
             'dsn' => 'mysql:host=127.0.0.1;dbname=esempio;charset=utf8',
             'username' => 'user',
             'password' => '', 
            ],
		 
         // api rate limiter settings
         'api_rate_limiter' =>
            [
             'requests' => '200000',
             'inmins' => '60',
            ],

         // monolog settings
         'logger' =>
            [
             'name' => 'app',
             'path' => __DIR__.'/../log/app.log',
            ],
        ],
    ];

?>