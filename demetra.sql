-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Set 22, 2018 alle 08:14
-- Versione del server: 5.7.23
-- Versione PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demetra`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('637cacea77c8b4c09cbeaffffb7a5fcb526a97c9', 'iCARE2606010', '260601', '2017-09-22 08:39:55', 'doctor');

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) NOT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(100) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('iCARE229520174444260601', '16201146505982dab59cf52', NULL, 'refresh_token password', 'basic', '229520174444'),
('iCARE2606010', '54254455058b81a6c5e8b6', NULL, 'refresh_token password', 'doctor', '260601'),
('iCARE407520172222260601', '16270355765982c6bb46431', NULL, 'refresh_token password', 'basic', '407520172222'),
('iCARE489920171111260601', '18778356885982c40a67a10', NULL, 'refresh_token password', 'basic', '489920171111'),
('iCARE502520170000260601', '204987644759881674bc3a7', NULL, 'refresh_token password', 'basic', '502520170000'),
('iCARE517720173333260601', '20294438615982daa105cb0', NULL, 'refresh_token password', 'basic', '517720173333'),
('iCARE553720173333260601', '17345211905982daa1205a6', NULL, 'refresh_token password', 'basic', '553720173333'),
('iCARE616120175555260601', '1207809924598819f622fda', NULL, 'refresh_token password', 'basic', '616120175555'),
('iCARE626120174444260601', '11121378755988187f7d106', NULL, 'refresh_token password', 'basic', '626120174444'),
('iCARE8610332432111111', '214382996358a43ef7f3c23', NULL, 'refresh_token password', 'basic', '8610332432'),
('iCARE999999', 'clientsecret', NULL, 'refresh_token, password', 'admin', '999999');

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`refresh_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('00157234ffe9b4810693b77e8dedb67d017a92c2', 'iCARE2606010', '260601', '2017-09-08 06:56:39', 'doctor'),
('01c7e5805d88abadb66e3ad29327091342755765', 'iCARE2606010', '260601', '2017-10-11 08:33:25', 'doctor'),
('06d4226e07363df6a445c405c936ff11df1be47e', 'iCARE2606010', '260601', '2017-08-22 06:40:27', 'doctor'),
('0869feace86e094cc393c6da7fb051ff8fc2887b', 'iCARE2606010', '260601', '2017-08-28 06:51:14', 'doctor'),
('0a8742f02a2e22b1e95fa3753b0b4cacdb05a58e', 'iCARE999999', '999999', '2017-08-31 08:02:40', 'admin'),
('0c26036fba602c0ac4155c4b2d62ca9217c5346a', 'iCARE999999', '999999', '2017-04-18 12:57:20', 'admin'),
('0d5dec0fc7ba2cc7251290725fa6bf8e99f98145', 'iCARE2606010', '260601', '2017-07-14 10:22:06', 'doctor'),
('1a0862059ec523ba42f618e5f2d43ed1076e1fdb', 'iCARE2606010', '260601', '2017-04-18 09:25:38', 'doctor'),
('1aad63dc3ef5a42529c9a2e5db23668bc2de1f22', 'iCARE2606010', '260601', '2017-06-22 13:05:44', 'doctor'),
('1caf8c5f8a7bcbca7af265a374451c2bcc43128e', 'iCARE2606010', '260601', '2017-06-28 12:10:00', 'doctor'),
('221c61500cdf46d81e5edffe7c77a475249b727e', 'iCARE616120175555260601', '616120175555', '2017-09-04 07:57:58', 'basic'),
('249fd216bffb644d193ed1bd7dd13395d4255056', 'iCARE2606010', '260601', '2017-05-18 13:58:59', 'doctor'),
('24a6c109f837b9cdb309fcc9b24ce38f2527c769', 'iCARE2606010', '260601', '2017-09-05 07:57:55', 'doctor'),
('27aa95ea439eeeca2cbc78e75bdf18e8f3883d70', 'iCARE2606010', '260601', '2017-06-19 08:20:22', 'doctor'),
('2875613129b986288bcbe5f774dba4d72995e1d2', 'iCARE616120175555260601', '616120175555', '2017-09-04 07:46:25', 'basic'),
('2f4f11155133dc4d83a8e8722df3aaf0e8a9a2cc', 'iCARE2606010', '260601', '2017-06-07 07:38:43', 'doctor'),
('2fa96e858aa693f63e7507154a427d3a1cb12398', 'iCARE999999', '999999', '2017-03-30 12:18:44', 'admin'),
('316148c7eabe36827e92cfabb88d4ac42894d3fb', 'iCARE2606010', '260601', '2017-06-14 13:03:53', 'doctor'),
('35b8e3ee2959593bc3bac8fd036b58ff3719ead4', 'iCARE2606010', '260601', '2017-08-25 10:06:16', 'doctor'),
('362186cc6e076217964d877d898561a9f2cda519', 'iCARE2606010', '260601', '2017-05-27 17:28:51', 'doctor'),
('3ddaaee8c257910d1c32c2750226a26590258449', 'iCARE2606010', '260601', '2017-05-28 18:03:23', 'doctor'),
('3e6c260da9f8a5741162347cb58c661b186739bc', 'iCARE2606010', '260601', '2017-06-02 08:22:30', 'doctor'),
('420073259c8819a0d78b55abce477526fd437373', 'iCARE2606010', '260601', '2017-06-21 08:47:10', 'doctor'),
('42d99696c7aabe83da455a1b64cafe06fd289706', 'iCARE8610332432111111', '8610332432', '2017-07-14 08:16:07', 'basic'),
('45456e78befa0f943832da731b9b780e12e5513e', 'iCARE2606010', '260601', '2017-07-01 17:08:23', 'doctor'),
('465c49c7eab73d7766a22331442916365156d425', 'iCARE8610332432111111', '8610332432', '2017-09-01 08:56:33', 'basic'),
('4721fc29e7d6c5c92f182fd7e9f75a7ba6d91ebc', 'iCARE616120175555260601', '616120175555', '2017-09-04 07:43:13', 'basic'),
('4753b4116bcd42dfacaf161eee463e4bc7f876c0', 'iCARE2606010', '260601', '2017-06-13 12:45:05', 'doctor'),
('4a66cedd153b5ef8c758e41f83b4534d772ff4dc', 'iCARE8610332432111111', '8610332432', '2017-09-19 14:05:11', 'basic'),
('4bc5a93665523cbfc1e92d49a467376e4898e9b7', 'iCARE2606010', '260601', '2017-09-26 14:01:46', 'doctor'),
('4ca4442523eec3bee836143487edc644e3ee0a96', 'iCARE2606010', '260601', '2017-08-24 07:24:16', 'doctor'),
('4ecffbbf1638c2a2f4f7ca366ade64321410d654', 'iCARE8610332432111111', '8610332432', '2017-09-02 09:11:53', 'basic'),
('4f4b295367eabe72ee9820117296e57bb4f8b3c3', 'iCARE2606010', '260601', '2017-09-25 07:05:06', 'doctor'),
('56b639faf5b2c2d6eace144a1c9fdb7557411af6', 'iCARE2606010', '260601', '2017-06-12 12:14:38', 'doctor'),
('58683084301c15a21addacc4e41efe099f28c845', 'iCARE2606010', '260601', '2017-06-29 13:34:40', 'doctor'),
('5ed4f1adf721e9050c87f3a0c9086dff824d9b99', 'iCARE2606010', '260601', '2017-04-06 09:51:42', 'doctor'),
('63d0059a51b34ebd8acababb0432494ef9f2fdd5', 'iCARE8610332432111111', '8610332432', '2017-09-25 07:06:22', 'basic'),
('65968597560b459e7e94355ecb98813273139cdc', 'iCARE502520170000260601', '502520170000', '2017-09-04 07:29:41', 'basic'),
('6876af7a5036fbd29df8e2fad0535a77608200a0', 'iCARE8610332432111111', '8610332432', '2017-04-17 09:02:12', 'basic'),
('69dde17aef2911695b4d36c2c36059bde2c31318', 'iCARE2606010', '260601', '2017-08-15 09:31:58', 'doctor'),
('6ee7b6a917109eb8b270eb019d80ada2aba801dd', 'iCARE2606010', '260601', '2017-10-19 08:39:55', 'doctor'),
('72e91bbcd91fb0c21a3abadcf3046a23c08b8e7b', 'iCARE8610332432111111', '8610332432', '2017-09-08 13:37:12', 'basic'),
('758113361b47da2a4a61b8f3036fce7c31cd6123', 'iCARE999999', '999999', '2017-09-02 08:08:32', 'admin'),
('7b9e6f06accafc98f5ef671e8f1c274f34c4826f', 'iCARE2606010', '260601', '2017-08-29 09:14:52', 'doctor'),
('7c8dedaf24db2f831d74363241ddd40b1da85311', 'iCARE8610332432111111', '8610332432', '2017-09-04 10:28:51', 'basic'),
('7e390a456037bd49dff0c0f0d3e34e43fe6b3333', 'iCARE999999', '999999', '2017-04-18 12:37:35', 'admin'),
('7fad38bedee5d689e6ff536676978374e52fff1b', 'iCARE2606010', '260601', '2017-08-23 07:22:48', 'doctor'),
('86f7f3e5a0afb99d74e31744e5deafcb0befb13c', 'iCARE2606010', '260601', '2017-04-24 13:18:51', 'doctor'),
('8afcfaf0337f19dd7849f209abc0f03b2d00e0fe', 'iCARE2606010', '260601', '2017-06-30 15:35:19', 'doctor'),
('8d9b1ee577d2fee0e915127acf8588ab58195c83', 'iCARE229520174444260601', '229520174444', '2017-08-31 08:16:55', 'basic'),
('8dd7437b5ff86d0a6027f9825f79d2452d871ddf', 'iCARE2606010', '260601', '2017-10-19 07:16:21', 'doctor'),
('8edb442f07cb8df19e0d71e1fa52a5349dba1009', 'iCARE489920171111260601', '489920171111', '2017-08-31 08:08:25', 'basic'),
('91a84c719930f271c681b7947ff7892ddd2826b1', 'iCARE2606010', '260601', '2017-04-18 10:14:59', 'doctor'),
('92adfdbfa8dbacc5190c5f43b78054b4ef286d14', 'iCARE2606010', '260601', '2017-04-07 09:22:48', 'doctor'),
('961a1f7984948b0175f1bd5a20ca28567015e86c', 'iCARE2606010', '260601', '2017-03-31 15:28:39', 'doctor'),
('9ecb6522fe1e9a9c9e54fef2e26b7a5af20fe63d', 'iCARE999999', '999999', '2017-10-11 07:56:40', 'admin'),
('a0be9b576342a0dbafda2d2bc1713845e1b55981', 'iCARE8610332432111111', '8610332432', '2017-04-05 12:50:51', 'basic'),
('a17275b37b2a4aeecd4e36d77ac7518adc634003', 'iCARE8610332432111111', '8610332432', '2017-08-21 13:22:22', 'basic'),
('a1bb36e2dfb6f5f912e86d20c6a3328e78dd64b5', 'iCARE489920171111260601', '489920171111', '2017-08-31 06:35:43', 'basic'),
('a3e638659aa03b55740fa54562194469325edb77', 'iCARE2606010', '260601', '2017-06-20 08:27:22', 'doctor'),
('a6141da56ab85c325ea3eec39c06becf32321a48', 'iCARE2606010', '260601', '2017-04-05 10:14:21', 'doctor'),
('a9ec4bdeaa4d0d1360286d4930e9e2cabe4b79b2', 'iCARE2606010', '260601', '2017-05-30 21:42:24', 'doctor'),
('b6c7e46a3cb8171debfa2d0bd5e41d02e5632006', 'iCARE2606010', '260601', '2017-06-27 09:07:05', 'doctor'),
('b6cf977f1e7dc7ca3df4ddf8262f3150e63e24d2', 'iCARE2606010', '260601', '2017-06-08 13:15:22', 'doctor'),
('ba7a67d84978f61cea3839f9ebea7c8f09a0437a', 'iCARE999999', '999999', '2017-08-30 06:40:20', 'admin'),
('bc6b698e0395ccf31769385fca5eab10126025c8', 'iCARE2606010', '260601', '2017-08-30 10:54:54', 'doctor'),
('c28db4261042b222d2c0826ff1874ab74f704709', 'iCARE8610332432111111', '8610332432', '2017-09-18 09:44:00', 'basic'),
('c2e0ed7acd5e830a8712276b706bd0d7d6b965ab', 'iCARE8610332432111111', '8610332432', '2017-09-05 10:59:53', 'basic'),
('c755514526ba3a77021897df00aba1724482d9b7', 'iCARE2606010', '260601', '2017-05-26 07:51:29', 'doctor'),
('c84f91fc3d67b7cbd5e7640047046d0a7c4ba8ea', 'iCARE2606010', '260601', '2017-04-17 08:39:35', 'doctor'),
('c891bc100143d14b6893333ca81699ca0879d163', 'iCARE2606010', '260601', '2017-07-04 09:24:07', 'doctor'),
('c8ea88c42d82ba2540ca49e96454fa5494c3907a', 'iCARE2606010', '260601', '2017-07-10 07:23:56', 'doctor'),
('cbf7e492938fe8b310e56b21a9cd612c8d7ecfcb', 'iCARE8610332432111111', '8610332432', '2017-03-23 10:30:22', 'basic'),
('ccc1e16234de66d6b30266c63308dd29172575c3', 'iCARE2606010', '260601', '2017-05-29 20:41:07', 'doctor'),
('cec8db6f9feec6ae5a11fafc6777b76f608d92ae', 'iCARE626120174444260601', '626120174444', '2017-09-04 07:37:54', 'basic'),
('d03002f9b1ddebe0ca1f7a5f733a8646655c9ce4', 'iCARE2606010', '260601', '2017-06-04 16:11:40', 'doctor'),
('d6f96e7b55cff50669580ea62408f9baaf1077b9', 'iCARE2606010', '260601', '2017-06-06 07:06:12', 'doctor'),
('d7072cde83291a92e56b46f51e5145442a5cc7d4', 'iCARE502520170000260601', '502520170000', '2017-09-04 07:32:17', 'basic'),
('dc813751a7559b6a3c71eee185b8146ba7b46159', 'iCARE2606010', '260601', '2017-09-04 07:27:13', 'doctor'),
('df33eca4d02743865256ed9069c8091f8c5a12ca', 'iCARE2606010', '260601', '2017-09-02 08:13:52', 'doctor'),
('e04ccf52606d2103381fa1e739a8de77b024aa37', 'iCARE999999', '999999', '2017-09-25 07:59:50', 'admin'),
('e0d3f54419940d339639c76f88c1e9f79ccd6449', 'iCARE2606010', '260601', '2017-07-02 17:20:29', 'doctor'),
('e14d8cf3e41ebff02e0bd05beca1f3015442c929', 'iCARE8610332432111111', '8610332432', '2017-07-05 09:32:57', 'basic'),
('e1b1ff5f5310fbc71592022ea457863785766858', 'iCARE2606010', '260601', '2017-07-11 07:31:28', 'doctor'),
('e63a315e31182202a473c515adb50c8c25118b79', 'iCARE8610332432111111', '8610332432', '2017-08-29 09:11:58', 'basic'),
('ed6f1735d1960ade5cd6aab92cf01cd4abef3199', 'iCARE2606010', '260601', '2017-08-03 14:05:18', 'doctor'),
('efcce43cdb0eb7c288c8458047c066c28885543b', 'iCARE8610332432111111', '8610332432', '2017-09-26 10:04:15', 'basic'),
('f212b96f9c3de56817c5b36b6bda321c17a60e1f', 'iCARE8610332432111111', '8610332432', '2017-10-10 09:20:06', 'basic'),
('f2525705c952beb85101121c75098b73711a139f', 'iCARE999999', '999999', '2017-05-27 18:25:43', 'admin'),
('f25a768dc9d1d307fa94d05df10177cdbc5ceafc', 'iCARE8610332432111111', '8610332432', '2017-07-04 08:34:41', 'basic'),
('f306a27fe20abb359f144f0c722a8404b5c9d8ec', 'iCARE8610332432111111', '8610332432', '2017-04-18 10:50:24', 'basic'),
('f5927e1fe1993c5b3114a4d1d668b25e8f703181', 'iCARE229520174444260601', '229520174444', '2017-08-31 08:12:44', 'basic'),
('fa844abfe042ef7341949115181d8b5fa78fda0a', 'iCARE2606010', '260601', '2017-07-12 12:29:57', 'doctor'),
('fc00cbf280b9d642d5500a0f5169e4baba62fac7', 'iCARE616120175555260601', '616120175555', '2017-09-04 07:43:52', 'basic'),
('fd294fedc62f7a926dca1e929b4b327c19488bfc', 'iCARE8610332432111111', '8610332432', '2017-03-31 09:05:31', 'basic'),
('fd5f643eb0942a78949f1407f6b0a40f2a517daf', 'iCARE626120174444260601', '626120174444', '2017-09-04 07:37:10', 'basic'),
('fe27eacca6aab7f265576f9217dac3662589a965', 'iCARE2606010', '260601', '2017-09-01 06:54:41', 'doctor'),
('fe3b816c1d7382fb59501706adbed1c875c402eb', 'iCARE2606010', '260601', '2017-06-26 08:46:13', 'doctor'),
('ff5804f8be5dbe25d6239a7fe41cc4d515d4ef23', 'iCARE616120175555260601', '616120175555', '2017-09-04 07:44:50', 'basic');

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` text,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Indici per le tabelle `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Indici per le tabelle `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indici per le tabelle `oauth_jwt`
--
ALTER TABLE `oauth_jwt`
  ADD PRIMARY KEY (`client_id`);

--
-- Indici per le tabelle `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
